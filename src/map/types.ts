import { LngLatLike } from "mapbox-gl";
import { Favorite } from "../components/Favorites/types";

export enum LayersLabel {
  POI_LABEL = 'poi-label'
}

export interface MapInterface {
  flyTo: (coords: LngLatLike) => void;
}

export interface UI {
  addPOI: (poi: Favorite) => void;
}