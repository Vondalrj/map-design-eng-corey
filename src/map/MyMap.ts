import mapboxGl, { LngLatLike, Map, Point } from "mapbox-gl";
import { LayersLabel, MapInterface, UI } from "./types";

const style = require('./data/style.json');

const defaultOptions = {
  style,
  center: [-77.04, 38.907],
  zoom: 15
};

export class MyMap implements MapInterface {
  public map: mapboxGl.Map;
  private ACCESS_TOKEN = 'pk.eyJ1IjoiZGFzdWxpdCIsImEiOiJjaXQzYmFjYmkwdWQ5MnBwZzEzZnNub2hhIn0.EDJ-lIfX2FnKhPw3nqHcqg';
  private ui: UI;
  private marker: null | mapboxGl.Marker;

  constructor(container: HTMLElement, ui: UI, options: object = defaultOptions) {
    mapboxGl.accessToken = this.ACCESS_TOKEN;
    this.map = new Map({...options, container});
    this.ui = ui;
    this.map.on('load', this.registerListeners);
    this.marker = null;
  }

  flyTo = (coords: LngLatLike) => {
    this.createMarker(coords);
    this.map.flyTo({center: coords, zoom: 15});
  };

  private registerListeners = () => {
    this.map.on('click', LayersLabel.POI_LABEL, (e) => {
      this.findAndAddPOI(e.point);
    });
  };

  private findAndAddPOI(point: Point) {
    const [feature] = this.map.queryRenderedFeatures(point, {layers: [LayersLabel.POI_LABEL]});
    if (feature && feature.properties) {
      this.ui.addPOI({
        id: Number(feature.id),
        name: feature.properties.name,
        type: feature.properties.type,
        coordinates: (feature.geometry as any).coordinates
      });
    }
  }

  private createMarker(coords: LngLatLike) {
    if (this.marker) {
      this.marker.remove();
    }

    const el = document.createElement('div');
    el.className = 'marker';

    this.marker = new mapboxGl.Marker(el)
      .setLngLat(coords)
      .addTo(this.map);
  }
}