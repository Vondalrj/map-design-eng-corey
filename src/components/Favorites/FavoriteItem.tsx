import { LngLatLike } from "mapbox-gl";
import * as React from "react";
import { Icon } from "../Icon";
import { Favorite } from "./types";

interface Props {
  favorite: Favorite;
  flyTo: (id: number, coords: LngLatLike) => void;
  selected: number | null;
  deleteFav: (id: number) => void;
}

export const FavoriteItem = ({favorite, flyTo, selected, deleteFav}: Props) => {
  const {id, coordinates, name, type} = favorite;
  return (
    <div
      className={selected === id ? 'selected favorite-row' : 'favorite-row'}
      data-testid="favorite"
      onClick={() => flyTo(id, coordinates)}
    >
      <div className="details" data-testid={name}>
        <div className="details-type">{type}</div>
        <div className="details-name">{name}</div>
      </div>
      <div data-testid="delete" onClick={(e) => {
        e.stopPropagation();
        deleteFav(id)
      }}>
        <Icon />
      </div>
    </div>
  );
};