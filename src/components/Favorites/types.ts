import { LngLatLike } from "mapbox-gl";

export interface Favorite {
  name: string;
  type: string;
  id: number;
  coordinates: LngLatLike;
}