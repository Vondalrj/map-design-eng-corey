import { LngLatLike } from "mapbox-gl";
import * as React from "react";
import { useState } from "react";
import { MapInterface } from "../../map/types";
import { FavoriteItem } from "./FavoriteItem";
import './FavoritesList.css';
import { Favorite } from "./types";

interface Props {
  favorites: Favorite[];
  map: MapInterface;
  deleteFav: (id: number) => void;
}


export const FavoritesList = ({deleteFav, favorites, map}: Props) => {
  const [selected, setSelected] = useState<number | null>(null);
  const flyTo = (id: number, coords: LngLatLike) => {
    setSelected(id);
    map.flyTo(coords)
  };

  const renderFavorites = () => {
    if (favorites.length === 0)  {
      return <div className="empty">Click a point on the map to add to your favorites.</div>;
    }

    return favorites.map((favorite) => {
      return <FavoriteItem
        key={favorite.id}
        favorite={favorite}
        flyTo={flyTo}
        selected={selected}
        deleteFav={deleteFav}
      />
    });
  };

  return (
    <div className="favorites-list">
      <h2>Favorites</h2>
      {renderFavorites()}
    </div>
  );
};