import { useState } from "react";
import * as React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { FavoritesList } from "./FavoritesList";
import { fireEvent } from '@testing-library/react';
import { Favorite } from "./types";

describe('Favorites', () => {
  let container: HTMLElement;
  const mapMock = {flyTo: jest.fn()};
  const firstPOI: Favorite = {id: 1, name: 'Rebellion', type: 'Restaurant', coordinates: [0, 0]};
  const secondPOI: Favorite = {id: 2, name: 'The Line', type: 'Hotel', coordinates: [0, 1]};

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
  });

  it('should render a list with a title "Favorites"', () => {
    act(() => {
      render(<FavoritesList favorites={[]} map={mapMock} deleteFav={() => {}}/>, container);
    });

    expect(container.textContent).toContain("Favorites");
    expect(container.textContent).toContain("Click a point on the map to add to your favorites.");
  });

  it('should show a list of favorited POIs', () => {
    act(() => {
      render(<FavoritesList favorites={[firstPOI]} map={mapMock} deleteFav={() => {}}/>, container);
    });

    expect(container.textContent).toContain('Rebellion');
    expect(container.textContent).toContain('Restaurant');

    act(() => {
      render(<FavoritesList favorites={[firstPOI, secondPOI]} map={mapMock} deleteFav={() => {}}/>, container);
    });

    expect(container.textContent).toContain('Rebellion');
    expect(container.textContent).toContain('Restaurant');
    expect(container.textContent).toContain('The Line');
    expect(container.textContent).toContain('Hotel');
  });

  it('should call map\'s flyTo with selecting a favorite', () => {
    act(() => {
      render(
        <FavoritesList
          favorites={[firstPOI, secondPOI]}
          map={mapMock}
          deleteFav={() => {}}
        />,
        container
      );
    });

    const favorite = container.querySelector("[data-testid='The Line']");
    fireEvent.click(favorite!);
    expect(mapMock.flyTo).toHaveBeenCalledWith([0, 1]);
  });

  it('should set the POI to selected after clicking on it', () => {
    act(() => {
      render(
        <FavoritesList
          favorites={[firstPOI, secondPOI]}
          map={mapMock}
          deleteFav={() => {}}
        />,
        container
      );
    });

    expect(container.querySelector('.selected')).toBeNull();
    const favorite = container.querySelector("[data-testid*='The Line']");
    fireEvent.click(favorite!);
    const selectedPOI = container.querySelector('.selected');
    expect(selectedPOI!.textContent).toContain('The Line');
  });

  it('should remove a location when the delete action is called', () => {
    const HighComp = () => {
      const [favs, setFavs] = useState<Favorite[]>([firstPOI, secondPOI]);
      return (<FavoritesList
        favorites={favs}
        map={mapMock}
        deleteFav={(id: number) => setFavs([secondPOI])}
      />)
    };

    act(() => {
      render(
        <HighComp/>,
        container
      );
    });

    let allFavorites = document.querySelectorAll("[data-testid*='favorite']");
    expect(allFavorites.length).toBe(2);

    const deleteAction = document.querySelector("[data-testid='delete']");
    fireEvent.click(deleteAction!);

    allFavorites = document.querySelectorAll("[data-testid*='favorite']");
    expect(allFavorites.length).toBe(1);
  });
});