### Overview

I had a lot of fun completing this exercise and getting more familiar with the Mapbox api!

The meat and potatoes of this app is split amongst two different directories, the `/map` and `/components` directory that are located within the `src` folder.

`map directory`: The `/map` directory holds the `MyMap` class which wraps the `mapboxGl` module and holds the logic for interacting with the map, type definitions, and includes supporting tests.

`components directory`: The `/components` directory houses the react components, view logic for interacting with the favorites list, type definitions for the components, some styles, and supporting tests
 
 ---
 
#### Mildly interesting tidbits
#### typescript


I decided to move this codebase over from javasript to typescript. I chose to use typescipt because I believe it helps with speed and confidence while exploring and developing new APIs. With typescript, the tooling support gives me quicker feedback to api definitions, inline refactors, and error prone code such as typos and undefined objects.

Typescript also leads to more "interesting" patterns. One of the patterns that I expiremented with in this codebase is called "Dependency Inversion Principle" (DIP). DIP is when the policy of how layers talk to one another is abstracted out into a contract or interface that both layers can adhere to.    


#### removing enzyme

I decided to remove enzyme and just use the test helpers supplied by react with jsdom. This approach led me to mount components to jsdom and then interact with the DOM directly to fire events, look for elements, and make my assertions based on inner html. I like this approach because it decouples behavior from implementation, allowing me to have confidence to refactor my code without having regressions in behavior. 

This approach is definitely do-able with enzyme but I felt like removing the library kept me from falling back on testing things like `props()`, `state()`, and class instances. 
### My approach

My approach was to seperate out all the **map logic** from **component logic**. My approach for doing that was to define an interface: the `MapInterface`. 
```
interface MapInterface {
  flyTo: (coords: LngLatLike) => void;
}

```
This allowed me to define the policy of how I wanted my react components to interact with the `MyMap` class and inject any implementation of `MapInterface` to the components.  

```
function App() {
    const [map, setMap] = useState<MapInterface | null>(null);
    const createMap = (container: HTMLElement) => {
        setMap(new MyMap(container, {...}))
    };
    <OtherComp map={map}/>
}

function OtherComp() {
    callBack = () => map.flyTo(coords)
}
```
This made sure that all components were only interacting with `MapInterface` and **not** an instance of `MyMap` directy.

The `MyMap` class then interacted directly with the `mapboxGl.Map` class. I think this is cool because if the `mapboxGl.Map` api changes, we are only modifying code in 1 place. Or, if we decide we wanted to use a whole other mapping library we would just create a new class that implements `MapInterface` and then inject that new class and not have to modify our react code.

On the other side, I created a `UI` interface which defined how `MapInterface` should interact with the `UI` and injected the implemtation into the constructor.
```
interface UI {
  addPOI: (poi: Favorite) => void;
}

class MyMap {
    constructor(private ui: UI) {}
    handClick(favorite) {
        this.ui.addPOI(favorite);
    }
}
```   

This gives us flexbility to switch from React to any other view implementation like Angular, Vue, or WebVR. As long as those new libraries implement an `#addPOI` method then nothing has to change in our `MyMap` class.

### Things I would have done with more time
With more time I would have implemented a few more things.
- De-duping favorites: right now you can add the same POI to the favorite list multiple times. It would be nice to only have unique POIs in the list
- use dependency injection for `mapboxGl.Map`: I would've like to inject an instance of `mapboxGl.Map` to `MyMap` class which would've given me better leverage to test `MyMap`. That way I could've created an Fake or Mock version of `mapboxGl.Map` and more indepthly test the event listeners
- conventions and consistency: With more time I would've like to clean up my components, classNames, and css to follow more conventions that make it easier for other developers to contribute to without having to make code style decisions  

### Commands
running the dev server

`yarn start`

running tests

`yarn test`
